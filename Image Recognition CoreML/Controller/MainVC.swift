//
//  ViewController.swift
//  Image Recognition CoreML
//
//  Created by parham khamsepour on 8/13/19.
//  Copyright © 2019 parham khamsepour. All rights reserved.
//

import UIKit
import AVKit
import Vision

class MainVC: UIViewController {

    @IBOutlet weak var cameraImage: UIImageView!
    @IBOutlet weak var objectLbl: UILabel!
    @IBOutlet weak var percentageLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSession()
        // Do any additional setup after loading the view.
    }
    func updateLbl(Label: UILabel, String: String) {
        Label.text = String
    }
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        guard let pixelBuffer: CVPixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else { return }
        guard let model = try? VNCoreMLModel(for: Inceptionv3().model) else { return }
        let request = VNCoreMLRequest(model: model) { (data, error) in
            // Checks if the data is in the correct format and assigns it to results
            guard let results = data.results as? [VNClassificationObservation] else { return }
            // Assigns the first result (if it exists) to firstObject
            guard let firstObject = results.first else { return }
            DispatchQueue.main.async {
                if firstObject.confidence * 100 >= 50{
                    self.objectLbl.text = firstObject.identifier.capitalized
                    self.percentageLbl.text = String(firstObject.confidence * 100) + "%"
                }

            }
            
            }
        
        try? VNImageRequestHandler(cvPixelBuffer: pixelBuffer, options: [:]).perform([request])

        }
}
extension MainVC: AVCaptureVideoDataOutputSampleBufferDelegate{
    func setupSession() {
        print("s")
        guard let device = AVCaptureDevice.default(for: .video) else { return }
        guard let input = try? AVCaptureDeviceInput(device: device) else { return }
        
        let session = AVCaptureSession()
        session.sessionPreset = .hd4K3840x2160
        let previewLayer = AVCaptureVideoPreviewLayer(session: session)
        previewLayer.frame = view.frame
        cameraImage.layer.addSublayer(previewLayer)
        
        let output = AVCaptureVideoDataOutput()
        output.setSampleBufferDelegate(self, queue: DispatchQueue(label: "videoQueue"))
        session.addOutput(output)
        // Sets the input of the AVCaptureSession to the device's camera input
        session.addInput(input)
        // Starts the capture session
        session.startRunning()
    }
}
